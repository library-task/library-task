<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Util\Util;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        try{
            return User::index();
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try{
            $instance = new User();
            $validation = Util::validationRules($request, $instance->new_rules());
            if (is_array($validation)) {
                return ['status' => false, 'message' => $validation[0]];
            }
            return User::store($request);
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $user){
        try{
            return User::show($user);
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
        try{
            $instance = new User();
            $validation = Util::validationRules($request, $instance->rules($request->id));
            if (is_array($validation)) {
                return ['status' => false, 'message' => $validation[0]];
            }
            return User::updateUser($request, $request->id);
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
        try{
            return User::destroy($request);
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public function login(Request $request){
        try{
            $parameters = $request->all();
            $user = User::where('email', $parameters["email"])
                ->where('role',1)
                ->first();
            if (!$user) {
                return ['status' => false, 'message' => 'Invalid credentials.'];
            }
            return User::login($request,$user);
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }
}
