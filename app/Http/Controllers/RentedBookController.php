<?php

namespace App\Http\Controllers;

use App\Models\RentedBook;
use App\Util\Util;
use Illuminate\Http\Request;

class RentedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        try{
            return RentedBook::index();
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        try{
            $instance = new RentedBook();
            $validation = Util::validationRules($request, $instance->rules());
            if (is_array($validation)) {
                return ['status' => false, 'message' => $validation[0]];
            }
            return RentedBook::store($request);
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RentedBook  $rentedBook
     * @return \Illuminate\Http\Response
     */
    public function show(RentedBook $rentedBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RentedBook  $rentedBook
     * @return \Illuminate\Http\Response
     */
    public function edit(RentedBook $rentedBook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RentedBook  $rentedBook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RentedBook $rentedBook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
        try{
            return RentedBook::destroy($request);
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }
}
