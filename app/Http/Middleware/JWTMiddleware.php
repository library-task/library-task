<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Auth;

class JWTMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            return $next($request);
        }

        $header = getallheaders();
        if(!isset($header["Authorization"])){
            return  response('Unauthorized Access', 403);
        }
        $token = $header["Authorization"];

        $token_bearer = trim($header["Authorization"]);
        if (!empty($token_bearer)) {
            if (preg_match('/Bearer\s(\S+)/', $token_bearer, $matches)) {
                $token =  $matches[1];
            }
        }
        else{
            return response('Unauthorized Access', 403);
        }
        try{
            return ['status'=> false, 'message'=>JWT::decode($token, 'library-crm', ['HS256'])];

        }
        catch (ExpiredException $e){
            abort(401,'Your session have expired. Please login again to continue');
        }
        catch(\Exception $e) {
            return response($e->getMessage(), 403);
        }
        return ['status'=> false, 'message'=>'Something went wrong, please try again later!'];
    }
}
