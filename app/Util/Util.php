<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/23/2022
 * Time: 12:08 AM
 */

namespace App\Util;


use Illuminate\Support\Facades\Validator;

class Util{

    public static function validationRules($request, $rules){
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validation = response()->json($validator->errors(), 422);
            return array_values((array)$validation->getData())[0];
        }
    }
}