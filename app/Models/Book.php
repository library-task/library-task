<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model{
    use SoftDeletes;
    public static function index(){
        try{
            $books = self::all();
            return [
                'status' => true,
                'message' => 'success',
                'data' => $books
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function store($parameters){
        try{
            $book = new self();
            $book_columns = $book->getTableColumns();

            foreach ($book_columns as $column) {
                if(isset($parameters[$column])) {
                    $book->{$column} = $parameters[$column];
                }
            }
            $is_saved = $book->save();
            if(!$is_saved){
                return ['status' => false, 'message' => 'Something went wrong.'];
            }

            return [
                'status' => true,
                'message' => 'success',
                'data' => $book
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function show(Book $book){
        if(!isset($book->id)){
            return ['status' => false, 'message' => 'Something went wrong.'];
        }
        $book = self::find($book->id);
        return [
            'status' => true,
            'message' => 'success',
            'data' => $book
        ];
    }

    public static function updateBook($parameters, $user_id){
        try{
            $book = self::find($user_id);
            $book_columns = $book->getTableColumns();
            foreach ($book_columns as $column) {
                if(isset($parameters[$column])) {
                    $book->{$column} = $parameters[$column];
                }
            }
            $is_saved = $book->save();
            if(!$is_saved){
                return ['status' => false, 'message' => 'Something went wrong.'];
            }

            return [
                'status' => true,
                'message' => 'success',
                'data' => $book
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function destroy($book){
        try{
            if(!isset($book->id)){
                return ['status' => false, 'message' => 'Something went wrong.'];
            }
            $book = self::find($book->id);
            if (!$book){
                return ['status' => false, 'message' => 'Book not found.'];
            }
            $is_deleted = $book->delete();
            if(!$is_deleted){
                return ['status' => false, 'message' => 'Book not deleted.'];
            }

            return [
                'status' => true,
                'message' => 'success'
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public function getTableColumns(){
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
