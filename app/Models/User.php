<?php

namespace App\Models;

use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class User extends Model{
    use SoftDeletes;

    public function rules($id){
        return [
            'number'  => 'required|unique:users,number,'.$id,
            'email'  => 'required|unique:users,email,'.$id
        ];
    }

    public function new_rules(){
        return [
            'number'  => 'required|unique:users,number',
            'email'  => 'required|unique:users,email'
        ];
    }

    public static function index(){
        try{
            $users = self::all();
            return [
                'status' => true,
                'message' => 'success',
                'data' => $users
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function store($parameters){
        try{
            $user = new self();
            $user_columns = $user->getTableColumns();

            foreach ($user_columns as $column) {
                if(isset($parameters[$column])) {
                    $user->{$column} = $parameters[$column];
                }
            }
            $is_saved = $user->save();
            if(!$is_saved){
                return ['status' => false, 'message' => 'Something went wrong.'];
            }

            return [
                'status' => true,
                'message' => 'success',
                'data' => $user
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function updateUser($parameters, $user_id){
        try{
            $user = self::find($user_id);
            $user_columns = $user->getTableColumns();
            foreach ($user_columns as $column) {
                if(isset($parameters[$column])) {
                    $user->{$column} = $parameters[$column];
                }
            }
            $is_saved = $user->save();
            if(!$is_saved){
                return ['status' => false, 'message' => 'Something went wrong.'];
            }

            return [
                'status' => true,
                'message' => 'success',
                'data' => $user
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function show($user){
        try{
            if(!isset($user->id)){
                return ['status' => false, 'message' => 'Something went wrong.'];
            }
            $user = self::find($user->id);
            return [
                'status' => true,
                'message' => 'success',
                'data' => $user
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function destroy($user){
        try{
            if(!isset($user->id)){
                return ['status' => false, 'message' => 'Something went wrong.'];
            }
            $user = self::find($user->id);
            if (!$user){
                return ['status' => false, 'message' => 'User not found.'];
            }
            $is_deleted = $user->delete();
            if(!$is_deleted){
                return ['status' => false, 'message' => 'User not deleted.'];
            }

            return [
                'status' => true,
                'message' => 'success'
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function login($credential, $user){
        try{
            if (Hash::check($credential["password"], $user->password)) {
                $login_data = self::loginData($user);
                return [
                    'status' => true,
                    'message' => 'success',
                    'data' => $login_data
                ];
            } else {
                return ['status' => false, 'message' => 'Wrong password'];
            }
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public function setPasswordAttribute($passAttr){
        try {
            if (isset($passAttr)) {
                $password = Hash::make($passAttr);
                $this->attributes['password'] = $password;
            }
        } catch (\Exception $exception) {
            $attribute['password'] = "";
        }
    }

    public static function loginData($user){
        return [
            'token' => self::jwt($user, 'library-crm'),
            'id' => $user->id,
            'email' => $user->email,
        ];
    }

    public static function jwt($user, $issuer){
        $payload = [
            'iss' => "$issuer", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 604800 // Expiration time 7 days
        ];
        return JWT::encode($payload, 'library-crm');
    }

    public function getTableColumns(){
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function rented_book(){
        return $this->hasMany(RentedBook::class,'user_id');
    }
}
