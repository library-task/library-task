<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class RentedBook extends Model{
    public $timestamps = true;
    use SoftDeletes;

    public function rules(){
        return [
            'user_id'  => 'required',
        ];
    }
    public static function index(){
        try{
            $book_rented_users = User::with('rented_book')
                ->where('is_rented_book', 1)->get();
            return [
                'status' => true,
                'message' => 'success',
                'data' => $book_rented_users
            ];
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function store($parameters){
        try{
            $exception = DB::transaction(function() use ($parameters) {
                $rented_book_arr = [];
                foreach ($parameters->book_ids as $book_id) {
                    $rented_book_arr[] = [
                        'book_id' => $book_id,
                        'user_id' => $parameters->user_id
                    ];
                }
                self::insert($rented_book_arr);
                User::where('id',$parameters->user_id)->update(['is_rented_book' => 1]);
            });

            if(is_null($exception)) {
                return [
                    'status' => true,
                    'message' => 'success'
                ];
            } else {
                throw new \Exception();
            }
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public static function destroy($user){
        try{
            if(!isset($user->id)){
                return ['status' => false, 'message' => 'Something went wrong.'];
            }
            $exception = DB::transaction(function() use ($user) {

                self::where('user_id',$user->id)->delete();
                User::where('id',$user->id)->update(['is_rented_book' => 0]);
            });

            if(is_null($exception)) {
                return [
                    'status' => true,
                    'message' => 'success'
                ];
            } else {
                throw new \Exception();
            }
        }catch (\Exception $exception) {
            return ['status' => false, 'message' => $exception->getMessage()];
        }
    }

    public function getTableColumns(){
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
