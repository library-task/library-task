<?php
use Illuminate\Support\Facades\Route;
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
Route::get('libraries/users/login', 'UserController@login');
Route::group(['middleware'=>'auth'], function () {
    Route::get('libraries/users', 'UserController@index');
    Route::post('libraries/users', 'UserController@store');
    Route::put('libraries/users/{id}', 'UserController@update');
    Route::get('libraries/users/{id}/get-user', 'UserController@show');
    Route::delete('libraries/users/{id}', 'UserController@destroy');

    Route::get('libraries/books', 'BookController@index');
    Route::post('libraries/books', 'BookController@store');
    Route::put('libraries/books/{id}', 'BookController@update');
    Route::get('libraries/books/{id}/get-book', 'BookController@show');
    Route::delete('libraries/books/{id}', 'BookController@destroy');


    Route::get('libraries/rental-books/users', 'RentedBookController@index');
    Route::post('libraries/rental-books', 'RentedBookController@store');
    Route::delete('libraries/rental-books/users/{id}', 'RentedBookController@destroy');
});