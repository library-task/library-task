<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        //php artisan db:seed --class=AdminUser
        $user = new User();
        $user->first_name = 'Admin';
        $user->last_name = 'Admin';
        $user->number = 434324324;
        $user->email = 'admin@gmail.com';
        $user->age = 30;
        $user->gender = 'm';
        $user->city = 'Mumbai';
        $user->role = 1;
        $user->password = 'admin';
        $dsf = $user->save();
    }
}
